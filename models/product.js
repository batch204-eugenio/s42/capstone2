const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	productName: {
		type: String,
		// Requires the data for this our fields/properties to be included when creating a record.
		// The "true" value defines if the field is required or not and the second element in the array is the message that will be printed out in our terminal when the data is not present
		required: [true, "productName is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number, 
		required: [true, "Price is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date().toLocaleString('default', {month : "short", weekday: "short"});
	},
	orders: [
		{
			orderId: {
				type: String,
				required: [true, "orderId is required"]
			},
			orderedOn: {
				type: Date,
				default: new Date().toLocaleString('default', {month : "short", weekday: "short"});
			}
		}
	]
});

//check if "Course" refers to a collection name in MongoDB
module.exports = mongoose.model("Product", productSchema);