const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		required : [true, "First name is required"]
	},
	lastName : {
		type : String,
		required : [true, "Last name is required"]
	},
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	mobileNo : {
		type : String, 
		required : [true, "Mobile No is required"]
	},
	order : [
		product : {
			type : String,
			required : [true, "Order is required"]
		},
		quantity : {
			type : Number,
			require : [true, "Quantity is required"]
		},
		totalAmount : {
			type : Number,
			require : [true, "Total amount is required"]	
		}, 
		purchasedOn : {
			type: Date,
			default: new Date().toLocaleString('default', {month : "short", weekday: "short"});
		}
	]
});
//check if "user" refers to a collection name in MongoDB
module.exports = mongoose.model("User", userSchema);

