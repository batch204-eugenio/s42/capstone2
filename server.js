const express = require("express");
const mongoose = require("mongoose");
// Allows our backend application to be available to our frontend application
// Allows us to control the app's Cross Origin Resource Sharing settings
const cors = require("cors");

//ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
// const userRoutes = require("./routes/userRoutes");
// const courseRoutes = require("./routes/courseRoutes");
//ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

const port = process.env.PORT || 4000;

const app = express();

//Connect to our MongoDB Database
mongoose.connect("mongodb+srv://admin:admin123@batch204.nanl81q.mongodb.net/s37-s41?retryWrites=true&w=majority", {
	useNewUrlParser: true,											  //hard coded
	useUnifiedTopology: true 										  //hard coded
});

//Setting mongoose.connection and checking once to confirm that it's now connected.
let db = mongoose.connection;
db.on("error", () => console.error.bind(console, "Error"));
db.once("open", () => console.log("Now connected to MongoDB Atlas!"));

// Allows all resources to access our backend application, unless if we have a whitelist.
app.use(cors());
app.use(express.json());
//ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
/*localhost:4000/users/checkEmail */
// app.use("/users", userRoutes);
// Defines the "/courses" string to be included for all course routes defined in the "course" route file
// app.use("/courses", courseRoutes);
//ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
//Listening now to port
app.listen(port, () => {
	console.log(`API is now online on port ${port}`);
});